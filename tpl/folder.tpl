<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Directory listing for {{ .Path }}</title>
</head>
<body>
<h1>Directory listing for {{ .Path }}</h1>
<hr>
{{ if .Items }}
<ul>
{{range .Items}}  <li><a href="{{ .Path }}">{{ .Name }}</a></li>
{{ end }}</ul>
{{ else }}
<div><strong>Nothing here.</strong></div>
{{ end }}
<hr>
</body>
</html>