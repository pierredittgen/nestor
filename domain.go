package main

import "path/filepath"

type FSItem struct {
	Path       string
	IsLink     bool
	IsDir      bool
	IsReadable bool
	Exist      bool
}

func (fs FSItem) Name() string {
	return filepath.Base(fs.Path)
}

func NewFSItem(path string, isLink, isDir, isReadable, exist bool) *FSItem {
	return &FSItem{path, isLink, isDir, isReadable, exist}
}
