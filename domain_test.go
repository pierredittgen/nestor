package main

import (
	"fmt"
	"testing"
)

func TestNew(t *testing.T) {

	tests := []struct {
		Path       string
		IsLink     bool
		IsDir      bool
		IsReadable bool
		Exist      bool
	}{
		{"/path/to/file", false, false, true, true},
		{"/path/to/link", true, false, true, false},
		{"/path/to/dir", true, true, true, true},
	}
	for i, tc := range tests {
		t.Run(fmt.Sprintf("NewFSItem=%d", i), func(t *testing.T) {

			fsItem := NewFSItem(tc.Path, tc.IsLink, tc.IsDir, tc.IsReadable, tc.Exist)
			if fsItem.Path != tc.Path {
				t.Errorf("Failed: Got path %v want %v", fsItem.Path, tc.Path)
			}
			if fsItem.IsLink != tc.IsLink {
				t.Errorf("Failed: Got isLink %v want %v", fsItem.IsLink, tc.IsLink)
			}
			if fsItem.IsDir != tc.IsDir {
				t.Errorf("Failed: Got isDir %v want %v", fsItem.IsDir, tc.IsDir)
			}
			if fsItem.IsReadable != tc.IsReadable {
				t.Errorf("Failed: Got isReadable %v want %v", fsItem.IsReadable, tc.IsReadable)
			}
			if fsItem.Exist != tc.Exist {
				t.Errorf("Failed: Got exist %v want %v", fsItem.Exist, tc.Exist)
			}
			t.Log("Success!")
		})
	}
}

func TestName(t *testing.T) {
	tests := []struct {
		Path string
		Name string
	}{
		{"/path/to/file.txt", "file.txt"},
		{"/etc/password", "password"},
		{"/etc", "etc"},
		{"/home/john", "john"},
	}
	for i, tc := range tests {
		t.Run(fmt.Sprintf("Name=%d", i), func(t *testing.T) {
			fsItem := FSItem{Path: tc.Path}
			if fsItem.Name() != tc.Name {
				t.Errorf("Failed: want %v got %v", tc.Name, fsItem.Name())
			}
			t.Log("Success!")
		})
	}
}
