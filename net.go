package main

import (
	"io"
	"log"
	"net/http"
	"path/filepath"
)

// ServeDir provides an HTML page listing subfolders and files of the given dir
func ServeDir(w io.Writer, fsItem FSItem, urlPath string) (err error) {

	fsItems, err := ComputeFSItemsFromDir(fsItem.Path, urlPath)
	if err != nil {
		return
	}
	return WriteFolderPage(w, urlPath, fsItems)
}

// ServeFile serves file content as an attachment
func ServeFile(w http.ResponseWriter, r *http.Request, fsItem FSItem) {
	http.ServeFile(w, r, fsItem.Path)
}

// HandleRequest serving current dir or file
func HandleRequest(w http.ResponseWriter, r *http.Request, rootDir string) (status int, urlPath string) {
	status = 200
	urlPath = r.URL.Path

	fsItem, err := fsItemFromPath(filepath.Join(rootDir, urlPath))
	if err != nil {
		status = WriteErrorPage(w, 500, "Internal server error")
		log.Println(err)
		return
	}
	if !fsItem.Exist {
		status = WriteErrorPage(w, 404, "Not found")
		return
	}
	if !fsItem.IsReadable {
		status = WriteErrorPage(w, 403, "Forbidden")
		return
	}

	// Directory => display directory content
	if fsItem.IsDir {
		err := ServeDir(w, *fsItem, urlPath)
		if err != nil {
			status = WriteErrorPage(w, 500, "Internal server error")
			log.Println(err)
		}
		return
	}

	// File => return file content
	ServeFile(w, r, *fsItem)
	return
}
