# nestor

Serve your local directory through http

This is a toy project aiming to learn [golang](https://go.dev/). Its name comes from a character of butler in a famous belgian comics.

## Install

If you already install go, use this command:

```bash
go install gitlab.com/pierredittgen/nestor@latest
```

otherwise download latest binary release for your platform at [pipeline page](https://gitlab.com/pierredittgen/nestor/-/pipelines), Artifacts button.

## Usage

Serve current directory on port 8000 (default)

```bash
nestor
```

See help to change directory, port or verbosity:

```bash
nestor -h
```
