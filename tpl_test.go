package main

import (
	"bytes"
	"fmt"
	"strconv"
	"strings"
	"testing"
)

func TestWriteErrorPage(t *testing.T) {
	tests := []struct {
		code int
		msg  string
	}{
		{403, "Forbidden"},
		{404, "Not found"},
		{500, "Internal server error"},
	}

	for i, tc := range tests {

		t.Run(fmt.Sprintf("WriteErrorPage=%d", i), func(t *testing.T) {

			var buffer bytes.Buffer
			returnCode := WriteErrorPage(&buffer, tc.code, tc.msg)
			textContent := buffer.String()

			if returnCode != tc.code {
				t.Errorf("Failed: got %v and return code, want %v", returnCode, tc.code)
			}
			if !strings.Contains(textContent, tc.msg) {
				t.Errorf("Failed: message %v doesn't appear in content %v", tc.msg, textContent)
			}
			if !strings.Contains(textContent, strconv.Itoa(tc.code)) {
				t.Errorf("Failed: http code %v doesn't appear in content %v", tc.code, textContent)
			}
			t.Logf("Success!")
		})
	}
}

func TestWriteFolderPageWithItems(t *testing.T) {

	inputPath := "/foo"
	inputItems := []FSItem{{Path: "/foo/a.txt"}}

	var buffer bytes.Buffer
	WriteFolderPage(&buffer, inputPath, inputItems)
	textContent := buffer.String()

	if !strings.Contains(textContent, inputPath) {
		t.Errorf("Failed: path %v not in result %v", inputPath, textContent)
	}
	itemPath := inputItems[0].Path
	if !strings.Contains(textContent, itemPath) {
		t.Errorf("Failed: item path %v not in result %v", itemPath, textContent)
	}
	itemName := inputItems[0].Name()
	if !strings.Contains(textContent, itemName) {
		t.Errorf("Failed: item name %v not in result %v", itemName, textContent)
	}
	if !strings.Contains(textContent, "<ul>") {
		t.Errorf("Failed: <ul> not found in result %v", textContent)
	}
	t.Log("Success!")
}

func TestWriteFolderPageWithoutItems(t *testing.T) {
	inputPath := "/foo"
	inputItems := []FSItem{}

	var buffer bytes.Buffer
	WriteFolderPage(&buffer, inputPath, inputItems)
	textContent := buffer.String()

	if !strings.Contains(textContent, inputPath) {
		t.Errorf("Failed: path %v not in result %v", inputPath, textContent)
	}
	if strings.Contains(textContent, "<ul>") {
		t.Errorf("Failed: <ul> found in result %v", textContent)
	}
	if !strings.Contains(textContent, "Nothing here") {
		t.Errorf("Failed: msg \"Nothing here\" not found in result %v", textContent)
	}
	t.Log("Success!")

}
