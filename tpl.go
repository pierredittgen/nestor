package main

import (
	_ "embed"
	"html/template"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
)

//go:embed tpl/folder.tpl
var FOLDER_TPL string

var folderTpl, _ = template.New("folder").Parse(FOLDER_TPL)

type FolderItem struct {
	Path string
	Name string
}

type FolderPageParams struct {
	Path  string
	Items []FSItem
}

func ComputeFolderItemsFromDir(dir string, path string) (items []FolderItem, err error) {
	files, err := ioutil.ReadDir(dir)
	if err != nil {
		return
	}
	for _, f := range files {
		fName := f.Name()
		fPath := filepath.Join(path, fName)
		items = append(items, FolderItem{Path: fPath, Name: fName})
	}
	return
}

func canRead(path string, isDir bool) bool {
	if isDir {
		_, err := os.ReadDir(path)
		return err == nil
	}
	fd, err := os.Open(path)
	defer fd.Close()
	return err == nil
}

func fsItemFromPath(path string) (*FSItem, error) {
	info, err := os.Stat(path)
	if err != nil {
		if os.IsNotExist(err) {
			return &FSItem{Path: path}, nil
		}
		return nil, err
	}
	isLink := info.Mode()&os.ModeSymlink != 0
	isDir := info.IsDir()

	return NewFSItem(path, isLink, isDir, canRead(path, isDir), true), nil
}

func ComputeFSItemsFromDir(dir string, path string) (items []FSItem, err error) {
	files, err := ioutil.ReadDir(dir)
	if err != nil {
		return
	}
	for _, f := range files {
		fPath := filepath.Join(path, f.Name())
		fsItem, err := fsItemFromPath(fPath)
		if err == nil {
			items = append(items, *fsItem)
		}
	}
	return
}

func WriteFolderPage(w io.Writer, urlPath string, fsItems []FSItem) error {
	return folderTpl.Execute(w, FolderPageParams{Path: urlPath, Items: fsItems})
}

//go:embed tpl/error.tpl
var ERROR_TPL string

var errorTpl, _ = template.New("error").Parse(ERROR_TPL)

type ErrorPageParams struct {
	Code int
	Msg  string
}

func WriteErrorPage(w io.Writer, httpCode int, message string) int {
	errorTpl.Execute(w, ErrorPageParams{httpCode, message})
	return httpCode
}
