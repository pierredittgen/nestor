package main

import (
	"errors"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
)

// Default port to use
const DEFAULT_PORT = 8000

// Directory to serve on /
var rootDir string

// Port to use
var port int

// Quiet mode
var quiet bool

var usage = fmt.Sprintf(`nestor: serve current directory content through HTTP

Usage : nestor [ OPTIONS ]
OPTIONS:
-p, --port       Set http port (default is %d)
-d, --directory  Set directory path (default is current directory)
-q, --quiet      Suppress messages
-h, --help       Prints help information
`, DEFAULT_PORT)

func main() {
	currentDir, err := os.Getwd()
	if err != nil {
		log.Fatal("Can't get current directory")
	}

	flag.IntVar(&port, "p", DEFAULT_PORT, "Set port number")
	flag.IntVar(&port, "port", DEFAULT_PORT, "Set port number")
	flag.StringVar(&rootDir, "d", currentDir, "Set directory path")
	flag.StringVar(&rootDir, "directory", currentDir, "Set directory path")
	flag.BoolVar(&quiet, "q", false, "Suppress messages")
	flag.BoolVar(&quiet, "quiet", false, "Suppress messages")
	flag.Usage = func() { fmt.Print(usage) }
	flag.Parse()

	info, err := os.Stat(rootDir)
	if errors.Is(err, os.ErrNotExist) {
		log.Fatalf("Directory not found: %s\n", rootDir)
	}
	if !info.IsDir() {
		log.Fatalf("Path is not a directory: %s", rootDir)
	}

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		status, urlPath := HandleRequest(w, r, rootDir)
		if !quiet {
			log.Printf("%d %s", status, urlPath)
		}
	})
	if !quiet {
		fmt.Printf("Serving %s on http://localhost:%d/...\n", rootDir, port)
	}

	// Deal with keyboard interrupt (CTRL-C)
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	go func() {
		<-c
		if !quiet {
			fmt.Println("Bye!")
		}
		os.Exit(0)
	}()

	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", port), nil))
}
